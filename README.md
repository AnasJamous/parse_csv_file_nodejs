## How this works:

1. the file is read bit by bit
2. the bits are assembled in lines
3. the lines are split on "," in 3 parts
4. the first part gets called "timestamp", the second "srcUser", the third "dstUser"
5. if the object "users" does not have a key "srcUser", then an array is created.
6. a small array of two items, [ dstUser, timestamp], get created
7. the array gets appended to the key "srcUser"

You end up with an object like this

{
srcUser: [ [ dstUser, timestamp], [ dstUser, timestamp], [ dstUser, timestamp], ...],
srcUser: [ [ dstUser, timestamp], [ dstUser, timestamp], [ dstUser, timestamp], ...]
srcUser: [ [ dstUser, timestamp], [ dstUser, timestamp], [ dstUser, timestamp], ...]
}

### Then when we finish reading the whole file, we just transform this big object into a string:

to do that, we:

1. join [dstUser, timeStamp] with "," and put "(" and ")" around it
2. join the user array with ","
3. now we have "(dstUser, timestamp), (dstUser, timestamp), ..."
4. then we add srcUser, so now we have "srcUser: (dstUser, timestamp), (dstUser, timestamp), ..."
5. then we join those with "\n"

We get:

srcUser: (dstUser, timestamp), (dstUser, timestamp), ..."
srcUser: (dstUser, timestamp), (dstUser, timestamp), ..."
srcUser: (dstUser, timestamp), (dstUser, timestamp), ..."

### How to run this code:

1- First you have to make sure that you have nodeJS installed on your machine.
2- Then open a project on your prefered IDE and do `npm install' 3- Finally just do`npm start' at the console and you will be able to see the result.
4- Any changes you will do it will automatically reflect without restarting the server.
