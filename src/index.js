const fs = require("fs");
const readline = require("readline");

/**
 * Just a simple wrapper around fs.watch.
 *
 * Windows throws a bunch of calls on every change, so this is debounced
 * @param {string} path
 * @param {Function} callback A function to run when a change occurs
 */
const watch = (path, callback) => {
  const makeDebouncedReaction = () => {
    let fsTimeout;
    return (eventType, filename) => {
      if (eventType === "change") {
        clearTimeout(fsTimeout);
        fsTimeout = setTimeout(callback, 100);
      }
    };
  };
  fs.watch(path, makeDebouncedReaction());
  callback();
};

/**
 * A streaming file reader which reads a CSV and indexes users
 * @param {string} filename the file to read
 */
const readCSV = (filename) =>
  new Promise((resolve, reject) => {
    // creates a stream will give you the file
    // little by little, not all at once. This way, the file doesn't fill the memory
    const inStream = fs.createReadStream(filename);
    // create a stream that reacts line by line
    const rl = readline.createInterface(inStream);

    const users = {};

    // called for each line
    rl.on("line", (line) => {
      const [timestamp, srcUser, dstUser] = line
        .split(",")
        .map((str) => str.trim());
      if (!users[srcUser]) {
        users[srcUser] = [];
      }
      users[srcUser].push([dstUser, timestamp]);
    });

    // called when the file is loaded
    rl.on("close", () => {
      let usersStrings = [];
      for (const userId in users) {
        const interactionList = users[userId]; // [ [ timeStamp, dstUser ], [ timeStamp, dstUser ], ...]
        const userInteractionsAsString = interactionList
          .map((interaction) => {
            const interactionStr = interaction.join(", ");
            return "(" + interactionStr + ")";
          })
          .join(", ");
        usersStrings.push(userId + ": " + userInteractionsAsString);
      }
      const str = usersStrings.join("\n");
      console.log(str);
    });
  });

const processTheFile = () => {
  readCSV("example.csv")
    .then(() => console.log("ok"))
    .catch((err) => {
      throw err;
    });
};

watch("example.csv", processTheFile);
